package org.example.dao;

import org.example.HibernateFactory;
import org.example.model.Actor;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ActorDao extends EntityDao<Actor> {
    private HibernateFactory hibernateFactory;

    public ActorDao(HibernateFactory hibernateFactory) {
        super(hibernateFactory, Actor.class);
        this.hibernateFactory = hibernateFactory;
    }

    public Actor getActorAndMovies(Integer id) {
        Session session = hibernateFactory.getSessionFactory().openSession();
        Actor actor = session.find(Actor.class, id);
        Hibernate.initialize(actor.getMovies());
        session.close();
        return actor;
    }


    public void hmm(){
        Session session = hibernateFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Actor actor = session.find(Actor.class, 1);
        actor.setName("Na pewno sie nie zmieni");
        transaction.commit();
        session.close();
//        actor.setName("Na pewno sie nie zmieni");
    }
}
