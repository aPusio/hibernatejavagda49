package org.example;

import org.example.dao.ActorDao;
import org.example.dao.EntityDao;
import org.example.dao.ReviewerDao;
import org.example.model.*;

import java.util.List;

public class MainRelationsExample {
    public static void main(String[] args) {
        HibernateFactory hibernateFactory = new HibernateFactory();
        ReviewerDao reviewerDao = new ReviewerDao(hibernateFactory, Reviewer.class);
        EntityDao<Movie> movieDao = new EntityDao<>(hibernateFactory, Movie.class);
        EntityDao<Author> authorDao = new EntityDao<>(hibernateFactory, Author.class);
        ActorDao actorDao = new ActorDao(hibernateFactory);

        savingOneToOne(reviewerDao, movieDao);
        savingOneToMany(movieDao, authorDao);
        savingManyToMany(actorDao, movieDao);

        actorDao.getActorAndMovies(1);

        Actor actor = actorDao.getById(1);
        List<Movie> movies = actor.getMovies();
        for (Movie movie : movies) {
            Author author = movie.getAuthor();

        }



        hibernateFactory.getSessionFactory().close();
    }

    private static void savingManyToMany(ActorDao actorDao, EntityDao<Movie> movieDao) {
        Actor michal = new Actor();
        michal.setName("Michal");
        michal.setSurname("Michalski");
        michal.setRating(Rating.MEDIUM);
        michal.setPesel("hahaha");

        Actor bartek = new Actor();
        bartek.setName("Bartek");
        bartek.setSurname("Bartkowiak");
        bartek.setRating(Rating.HIGH);

        Movie titanic2 = new Movie();
        titanic2.setTitle("Titanic II");

        Movie titanic3 = new Movie();
        titanic3.setTitle("Titanic III");

        titanic2.setActors(List.of(michal, bartek));

        actorDao.save(michal);
        actorDao.save(bartek);
        movieDao.save(titanic2);
        movieDao.save(titanic3);
    }

    private static void savingOneToMany(EntityDao<Movie> movieDao, EntityDao<Author> authorDao) {
        Author author = new Author();
        author.setName("Someone");
        author.setSurname("Crazy");

        Movie titanic = new Movie();
        titanic.setTitle("titanic");
        titanic.setAuthor(author);

        Movie psiPatrol = new Movie();
        psiPatrol.setTitle("Psi Patrol");
        psiPatrol.setAuthor(author);

//        not needed
//        author.setMovieList(List.of(titanic, psiPatrol));
        authorDao.save(author);
        movieDao.save(titanic);
        movieDao.save(psiPatrol);
    }

    private static void savingOneToOne(ReviewerDao reviewerDao, EntityDao<Movie> movieDao) {
        Movie sampleMovie = new Movie();
        sampleMovie.setTitle("Sample movie");
        sampleMovie.setProductionYear(1997);
        sampleMovie.setType("DRAMA");

        Reviewer reviewer = new Reviewer();
        reviewer.setName("Adam");
        reviewer.setSurname("Adamski");
        reviewer.setMovie(sampleMovie);

        movieDao.save(sampleMovie);
        reviewerDao.save(reviewer);
        Reviewer byId = reviewerDao.getById(reviewer.getId());
        System.out.println(byId);
    }
}
