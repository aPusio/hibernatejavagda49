package org.example.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Actor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Length(min = 77)
    private String name;
    private String surname;
    private Integer yearsOfExperience;
    @Enumerated(value = EnumType.STRING)
    private Rating rating;
    @Email
    private String email;

    @PESEL(message = "a moze byc tak poprawny pesel ? ")
    private String pesel;

    //eager zawsze ? no chyba nie
//    @ManyToMany(mappedBy = "actors", fetch = FetchType.EAGER)
    @ManyToMany(mappedBy = "actors")
    private List<Movie> movies;
}
